# helm-kubectl-docker
docker image with helm and kubectl installed

Based on alpine

Docker: https://hub.docker.com/r/nickrosener/helm-kubectl-docker/

|Tag        | Alpine |   Kubectl    |  Helm |
|:---------:|:------:|:------------:|:-----:|
|latest     |3.8     |1.13.0        |2.12.0 |
|v1130_2120 |3.8     |1.13.0        |2.12.0 |

Fork of https://github.com/lwolf/helm-kubectl-docker